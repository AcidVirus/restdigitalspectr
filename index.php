<?php
require __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/src/configs/db.php';

use Bramus\Router\Router;

$router = new Router();
$router->setNamespace('\App\Controllers');
$router->get('/feedback', 'FeedbackController@index');
$router->post('/feedback/add', 'FeedbackController@add');
$router->get('/geo-ip', 'GeoIpController@index');
$router->run();
