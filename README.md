# REST приложение для DigitalSpectr #
## Требование к окружению ##
1. Версия PHP >= 7.2 
1. Установленный composer для установки зависимостей
1. РСУБД(MySQL, PostgreeSQL и т.д.)
 
## Настройка ##
1. Импортировать в базу данных таблицу "feedback", которая находится в папке db.
1. Прописать данные для подключеня к базе данных в конфигурационном файле src/configs/db.php 
1. Запустить команду composer update, для установки зависимостей

## Как пользоваться приложением ##
#### Формат ответа ####
~~~
{
    "isError":<bool>, // Статус ответа сервера. True = ошибка
    "data":<object> // Ответ на запрос.
}
~~~

#### Методы работы с сообщениями ####
1. GET /feedback - открыть форму отправки обратной связи.
1. POST /feedback/add - добавить данные в таблицу 'feedback'.

##### Параметры #####
Параметры принимаются в виде JSON объекта

* phone (обязательный) - номер телефона
* email (обязательный) - email адрес. Должен соответстовать формату email адреса. Например user@domain.com
* message (не обязательный) - текст сообщения

#### Возвращаемые значения ####
##### Успешное выполнение #####
* При успешном сохранении, возвращаются данные, сохраненного сообщения
~~~
{
  "isError": false,
  "data": {
    "id": <feedback_id>,
    "phone": "<feedback_phone>",
    "email": "feedback_email",
    "message": "feedback_message"
  }
}
~~~
##### Ошибки при выполнении #####
* Не настроен доступ к базе данных
~~~
{
  "isError": true,
  "data": "invalid data source name"
}
~~~

* Обязательные поля не заполнены
~~~
{
  "isError": true,
  "data": [
    {
      "field": "phone",
      "message": "Поле должно быть заполнено"
    },
    {
      "field": "email",
      "message": "Поле должно быть заполнено"
    }
  ]
}
~~~

* Значение в поле email не соответствует формату электронной почты
~~~
{
  "isError": true,
  "data": [
    {
      "field": "email",
      "message": "Заполните поле в соответствии с форматом \"user@domain.com\""
    }
  ]
}
~~~
---
#### Метод получения географического положения по ip ####
GET /geo-ip - получить географическое положение клиента, по ip адресу.
##### Параметры #####
* ip (необязательный) - ip адрес. Если ip не указан, для поиска будет использоваться ip адрес клиента, выполнившего запрос 
#### Возвращаемые значения ####
* При успешном выполнении, возврщаются данные о географическом положении клиента
~~~
{
  "isError": false,
  "data": {
    "ResolveIPResult": {
      "City": "<value>",
      "StateProvince": "<value>",
      "Country": "<value>",
      "Organization": "<value>",
      "Latitude": <value>,
      "Longitude": <value>,
      "AreaCode": "<value>",
      "TimeZone": "<value>",
      "HasDaylightSavings": <value>,
      "Certainty": <value>,
      "RegionName": "<value>",
      "CountryCode": "<value>"
    }
  }
}
~~~

* Система не нашла запрашиваемый ip адрес
~~~
{
  "isError": true,
  "data": "Клиент с указанным ip адресом не был найден"
}
~~~

* Истек пробный период ¯\_(ツ)_/¯
~~~
{
  "isError": true,
  "data": "Пробный период истек. В течении 2-ух часов сервис будет доступен"
}
~~~