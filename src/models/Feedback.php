<?php

namespace App\models;

use App\classes\Model;
use Nette\Database\IRow;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\Validation;

/**
 * Model for table "feedback"
 *
 * @property integer $id
 * @property string $phone
 * @property string $email
 * @property string $message
 *
 * @property-read array $errors
 * */
class Feedback extends Model
{
    private $id;
    private $phone;
    private $email;
    private $message;

    private $errors;

    public static function tableName(): string
    {
        return 'feedback';
    }

    public function __get($name)
    {
        return $this->{$name};
    }

    public function __set($name, $value) : void
    {
        switch ($name) {
            case 'phone':
                $validator = Validation::createValidator();
                $violations = $validator->validate($value, [
                    new NotBlank([
                        'message' => 'Поле должно быть заполнено'
                    ]),
                ]);
                if (count($violations)) {
                    foreach ($violations as $violation) {
                        $this->addViolations('phone', $violation);
                    }
                } else {
                    $this->phone = $value;
                }
                break;
            case 'email':
                $validator = Validation::createValidator();
                $violations = $validator->validate($value, [
                    new NotBlank([
                        'message' => 'Поле должно быть заполнено'
                    ]),
                    new Email([
                        'message' => 'Заполните поле в соответствии с форматом "user@domain.com"'
                    ])
                ]);
                if (count($violations)) {
                    foreach ($violations as $violation) {
                        $this->addViolations('email', $violation);
                    }
                } else {
                    $this->email = $value;
                }
                break;
            case 'message':
                $this->message = $value;
                break;
            default:
                throw new \InvalidArgumentException('Попытка указать несуществующее свойство');
        }
    }

    public function __isset($name)
    {
        return property_exists($this, $name);
    }

    public function save(): IRow
    {
        if ($this->id) {
            return $this->update($this->id, [
                'phone' => $this->phone,
                'email' => $this->email,
                'message' => $this->message,
            ]);
        }

        return $this->insert([
            'phone' => $this->phone,
            'email' => $this->email,
            'message' => $this->message,
        ]);
    }

    private function addViolations(string $fieldName, ConstraintViolation $violation): void
    {
        $this->errors[] = [
            'field' => $fieldName,
            'message' => $violation->getMessage(),
        ];
    }
}
