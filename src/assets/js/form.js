$(document).ready(function () {
    $('body').on('click', '#submit', function(){
        let phone = $('#phone').val();
        let email = $('#email').val();
        let message = $('#message').val();

        $.post({
            url: 'feedback/add',
            data: {
                phone: phone,
                email: email,
                message: message,
            },
            success: function (result) {
                if (result.isError) {
                    result.data.forEach((val) => {
                        $('#' + val.field).addClass('error').parent().find('.error-label').text(' - ' + val.message)
                    })
                } else {
                    $('#success-modal').modal('show');
                    $('#feedback-form input, textarea').val('');
                }
            },
            error: function (xhr, status, error) {
                console.log(error)
            }
        })
    })
});
