<?php

namespace App\Controllers;

use App\classes\Controller;
use App\models\Feedback;

class FeedbackController extends Controller
{
    public function index(): void
    {
        $this->render('index');
    }

    public function add(): void
    {
        $request = $this->request();
        $phone = $request->phone;
        $email = $request->email;
        $message = $request->message;

        header('Access-Control-Allow-Origin: *');
        header('Content-type: application/json');
        try {
            $feedback = new Feedback();
            $feedback->phone = $phone;
            $feedback->email = $email;
            $feedback->message = $message;

            if ($feedback->errors) {
                $this->response($feedback->errors, true);
            } else {
                $this->response($feedback->save());
            }
        } catch (\Exception $e) {
            $this->response($e->getMessage(), true);
        }
    }
}
