<?php

namespace App\Controllers;

use App\classes\Controller;
use SoapClient;

class GeoIpController extends Controller
{
    public function index(): void
    {
        header('Access-Control-Allow-Origin: *');
        header('Content-type: application/json');

        $request = $this->request();

        $url = "http://ws.cdyne.com/ip2geo/ip2geo.asmx?WSDL";
        $ip = $request->ip ? $request->ip : $_SERVER['REMOTE_ADDR'];
        $params = [
            'licenseKey' => 0,
            'ipAddress' => $ip,
        ];

        try {
            $client = new SoapClient($url);
        } catch (\SoapFault $e) {
            $this->response('Не удалось установить соединение с сервисом определения географического положения', 1);
            exit;
        }

        $result = $client->ResolveIP($params)->ResolveIPResult;

        if ($result->Country) {
            $this->response($result);
            exit;
        }

        switch ($result->Organization) {
            case "Not Found":
                $this->response('Клиент с указанным ip адресом не был найден', true);
                break;
            case "Please Obtain a License Key to Test Further or wait 2 hours and you can test more.":
                $this->response('Пробный период истек. В течении 2-ух часов сервис будет доступен', true);
                break;
            default:
                $this->response($result, true);
        }
    }
}
