<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Форма обратной связи | DigitalSpectr</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="stylesheet" href="src/assets/css/main.css">
</head>
<body class="bg-light vh-100">
    <div class="container vh-100 w-50">
        <div class="row d-inline align-middle">
            <form class="col-12 card card-block pt-2" id="feedback-form">
                <h2>Форма обратной связи</h2>
                <hr>
                <div class="form-group">
                    <label class="required" for="email">E-mail <span class="error-label"></span></label>
                    <input class="form-control" type="text" name="email" id="email">
                </div>
                <div class="form-group">
                    <label class="required" for="phone">Телефон <span class="error-label"></span></label>
                    <input class="form-control" type="text" name="phone" id="phone">
                </div>
                <div class="form-group">
                    <label for="message">Сообщение</label>
                    <textarea class="form-control" name="message" id="message" cols="15" rows="10"></textarea>
                </div>
                <div class="form-group">
                    <a id="submit" class="btn btn-success col-12 text-white">Отправить</a>
                </div>
            </form>
        </div>
    </div>

    <div class="modal fade" data-backdrop="static" id="success-modal" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12 text-center">
                            <h3>
                                Сообщение успешно отправлено!
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" data-toggle="modal" class="btn btn-secondary col-12">Закрыть</button>
                </div>
            </div>

        </div>
    </div>

    <script src="src/assets/js/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
    <script src="src/assets/js/main.js"></script>
    <script src="src/assets/js/form.js"></script>
</body>
</html>