<?php

namespace App\classes;

/**
 * Base controller class
 */
class Controller
{
    /**
     * Method returns request params
     *
     * @return object
     */

    public function request()
    {
        if ($_SERVER['CONTENT_TYPE'] === 'application/json') {
            return json_decode(file_get_contents('php://input'), false);
        }

        if ($_SERVER['REQUEST_METHOD'] === 'GET') {
            return (object) $_GET;
        }

        return (object) $_POST;
    }

    /**
     * Method return to client formatted response in JSON
     *
     * @param array|string $data
     * @param bool $isErr
     */
    public function response($data = null, bool $isErr = false) : void
    {
        $result = [
            'isError' => $isErr,
            'data' => $data,
        ];
        echo json_encode($result);
    }

    /**
     * Render view
     *
     * @param string $viewName name format is "view" or "controller/view"
     */
    public function render(string $viewName) : void
    {
        $viewNameParts = explode('/', $viewName);
        if (count($viewNameParts) > 1) {
            $controllerName = $viewNameParts[0];
        } else {
            $backtrace = debug_backtrace();
            $parts = explode('\\', $backtrace[1]['class']);
            $controllerName = mb_strtolower(str_replace('Controller', '', array_pop($parts)));
        }

        include_once __DIR__ . '/../views/' . $controllerName . '/' . $viewName . '.php';
    }
}
