<?php

namespace App\classes;

use Nette\Database\Connection;
use Nette\Database\IRow;

/**
 * Base model class
 */
class Model
{
    private $db;

    public function __construct()
    {
        $this->db = new Connection(DB_DSN, DB_USER, DB_PASS, DB_OPTIONS);
    }

    public static function tableName() : string
    {
        return '';
    }

    public function getLast() : IRow
    {
        $table = static::tableName();
        return $this->db->fetch('SELECT * FROM ?name ORDER BY id DESC LIMIT 1', $table);
    }

    public function getById(int $id) : IRow
    {
        $table = static::tableName();
        return $this->db->fetch('SELECT * FROM ?name WHERE id = ?', $table, $id);
    }

    public function insert(array $data) : IRow
    {
        $table = static::tableName();
        $this->db->query('INSERT INTO ?name ?', $table, $data);
        return $this->getLast();
    }

    public function update(int $id, array $data) : IRow
    {
        $table = static::tableName();
        $this->db->query('UPDATE ?name  SET', $table, $data, 'WHERE id = ?', $id);
        return $this->getById($id);
    }
}
